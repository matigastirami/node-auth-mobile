import React from "react";
import { View } from "react-native";
import AppContainer from './src/navigation/AppContainer'


export default class App extends React.Component {

	constructor(props) {
		super(props)

		this.state = {};
	}

	render() {
		return (
			<View style={{flex: 1, alignContent: 'center', justifyContent: 'center'}}>
				<AppContainer/>
			</View>
		);
	}
}
