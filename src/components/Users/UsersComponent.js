import React, { Component } from 'react';
import { View, Text } from 'react-native';
import CRUDComponent from '../CRUDComponent/CRUDComponent'

export default class UsersComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  static navigationOptions = {
    title: 'Gestión de usuarios'
  };

  render() {
    return (
      <View>
        <CRUDComponent
          class={''}
          controller={'user'}
          pk={'_id'}
          clickCallback={() => console.log('Prueba Click Callback')}
          enableSelect={false} //Pendiente implementar (checkbox a la izquierda de las columnas)
          enableCreate={true}
          enableUpdate={true}
          enableDelete={true}
          enableCache={false} //Pendiente implementar
          enableExport={[false, ['csv', 'xml', 'json']]} //Pendiente implementar
          enablePrint={false} //Pendiente implementar
          enableSorting={false} //Pendiente implementar
          enableColumnFilter={false} //Pendiente implementar
          itemsPerPage={10}
          filter={[
            { 'parameterName': 'username', 'parameterValue': '', 'parameterContainer': 'query' }
          ]}
          columns={[
            { 'columnName': 'username', 'title': 'Usuario' },
            { 'columnName': 'email', 'title': 'Email' }
          ]}
          additionalActions={[

          ]}
        />
      </View>
    );
  }
}
