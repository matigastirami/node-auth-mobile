import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import PropTypes from 'prop-types'
import serviceCommunication from '../../http/genericComunication'
import { getFromStorage } from '../../Storage/Storage'
import { DataTable } from 'react-native-paper';


class CRUDComponent extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			activePage: 1,
			queryFilters: {},
			bodyFilters: {},
			columns: [],
			currentItems: [],
			items: [],
			searchTerm: '',
			showModalDelete: false,
			rowToDelete: null
		}

		this.handlePageChange = this.handlePageChange.bind(this)

	}

	componentDidMount() {
		this.processFilters()
			.then((resolved) => {
				this.getColumnsToShow()
				this.getData()
			})
	}

	isColumnToShow = (colName) => {
		return this.state.columns.indexOf(colName) != -1;
	}

	getColumnsToShow = () => {
		var columnsToShow = []
		this.props.columns.map((item) => {
			columnsToShow.push(item["columnName"])
		})

		console.log(columnsToShow)

		this.setState({
			columns: columnsToShow
		})
		this.forceUpdate()
	}

	componentWillReceiveProps(nextProps) {
		this.processFilters()
			.then(() => {
				this.getData()
			})
	}

	processFilters = () => {
		return new Promise((resolve) => {
			var body = {}
			var query = {}

			var filters = this.props.filter

			filters.forEach((filter) => {
				if (filter.parameterValue && filter.parameterValue != '') {
					if (filter.parameterContainer == 'query')
						query[filter.parameterName] = filter.parameterValue
					else
						body[filter.parameterName] = filter.parameterValue
				}
			})

			this.setState({
				queryFilters: query,
				bodyFilters: body
			})
			this.forceUpdate()

			resolve()
		})
	}

	getData = () => {
		getFromStorage('SessionInfo')
		.then(SessionInfo => {
			serviceCommunication.get(this.props.controller, this.state.queryFilters, this.state.bodyFilters, null, JSON.parse(SessionInfo).token)
			.then(resp => {

				let filteredFields = []

				resp.data.forEach((item) => {
					var filteredField = []
					for (var propertyName in item) {
						if (propertyName == this.props.pk) {
							filteredField.splice(0, 0, item[propertyName])
						}
						if (this.isColumnToShow(propertyName)) {
							filteredField.push(item[propertyName])
						}
					}
					filteredFields.push(
						filteredField
					)
					filteredField = []
				})

				this.setState({
					items: filteredFields,
					currentItems: filteredFields.slice(this.state.activePage - 1, this.state.activePage + this.props.itemsPerPage - 1)
				})
				this.forceUpdate()
			},
				err => {
					console.log("Ocurrió un error al recuperar los datos: ", JSON.stringify(err))
					//alert(err.response.data.message)
				})
		},
		error => {
			console.log("Ocurrió un error", JSON.stringify(err))
			alert("ERROR")
		})
	}

	handleDelete = (pk) => {
		this.setState({
			showModalDelete: true,
			rowToDelete: pk
		})
	}

	handleEdit = (pk) => {
		HashHistory.push('/' + this.props.controller + '/' + pk)
	}

	handleNewAction = () => {
		HashHistory.push('/' + this.props.controller + '/new')
	}

	onCancelDelete = () => {
		this.setState({
			showModalDelete: false
		})
		this.forceUpdate()
	}

	onConfirmDelete = () => {
		serviceCommunication.delete(this.props.controller, this.state.rowToDelete)
			.then(
				resp => {
					alert(resp.data.message)
					this.getData()
					this.setState({
						rowToDelete: null,
						showModalDelete: false
					})
					this.forceUpdate()
				},
				err => {
					alert(err.response.data.message)
				}
			)
	}

	handlePageChange(page) {

		let init = (page - 1) * this.props.itemsPerPage
		this.setState({
			show: false,
			activePage: page,
			currentItems: this.state.items.slice(init, init + this.props.itemsPerPage),
			searchTerm: ''
		})
		this.forceUpdate()
	}

	handleSearchTermChange = (ev) => {

		this.setState({
			[ev.target.id]: ev.target.value
		})

		let currItems = []

		if (ev.target.value.length >= 3 && this.state.items) {
			var re = new RegExp(ev.target.value, 'gi')

			this.state.items.filter(item => {
				for (let i = 0; i < item.filteredField.length && !match; i++) {
					var match = false
					if (item.filteredField[i].match(re)) {
						currItems.push(item)
						match = 1
					}
				}
			})
			this.setState({
				currentItems: currItems
			})
			this.forceUpdate()
		}
		else {
			this.setState({
				currentItems: this.state.items.slice((this.state.activePage - 1) * this.props.itemsPerPage, (this.state.activePage - 1) * this.props.itemsPerPage + this.props.itemsPerPage)
			})
			this.forceUpdate()
		}


	}


	render() {
		return (
			<View style={{flex: 1, alignContent:"center", justifyContent: 'center',}}>
				<DataTable>
					<DataTable.Header>
					{
						this.props.columns.map((col, i) => {
							return <DataTable.Title key={i}>{col.title}</DataTable.Title>
						})	
					}
						<DataTable.Title>Acciones</DataTable.Title>
					</DataTable.Header>

					{
						this.state.items.map((item, rowIndex) => {
							return (
								<DataTable.Row key={rowIndex}>
									{
										item.slice(1).map((cell, cellIndex) => {	
											return <DataTable.Cell key={cellIndex}>{cell}</DataTable.Cell>
										})
									}
									<DataTable.Cell >					
									{
										this.props.additionalActions.map((action, i) => {
											return (
												<Button title={action.tooltip} onPress={action.callback()}/>
											)
										})
									}
									</DataTable.Cell>
								</DataTable.Row>
							)
						})

					}

					<DataTable.Pagination
						page={1}
						numberOfPages={3}
						onPageChange={(page) => { console.log(page); }}
						label="1-2 of 6"
					/>
				</DataTable>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
	head: { height: 40, backgroundColor: '#f1f8ff' },
	text: { margin: 6 }
});

CRUDComponent.propTypes = {
    class: PropTypes.string,
    controller: PropTypes.string.isRequired,
    pk: PropTypes.string.isRequired,
    clickCallback: PropTypes.func,
    enableSelect: PropTypes.bool.isRequired,
    enableCreate: PropTypes.bool.isRequired,
    enableUpdate: PropTypes.bool.isRequired,
    enableDelete: PropTypes.bool.isRequired,
    enableCache: PropTypes.bool.isRequired,
    filter: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    additionalActions: PropTypes.array
}

export default CRUDComponent;