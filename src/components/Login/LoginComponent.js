import React, { Component } from 'react';
import { View, AsyncStorage, Text } from 'react-native';
import LoginService from './Login.service'
import { TextInput, Button } from 'react-native-paper';

export default class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        username: '',
        password: ''
    }
  }

  _storeData = async (itemName, itemValue) => {
    try {
      await AsyncStorage.setItem(itemName, JSON.stringify(itemValue));
    } catch (error) {
      alert("Error al guardar datos de sesión")
      console.log(error)
    }
  };

  handleLogin = () => {
      //alert("Bienvenido " + this.state.username)
      if(this.state.username == ''){
        alert("Debe ingresar un nombre de usuario")
        return;
      }
      if(this.state.password == ''){
        alert("Debe ingresar una contraseña")
        return;
      }
      LoginService.signin(this.state.username, this.state.password)
        .then(res => {
          this._storeData("SessionInfo", res.data)
          this.props.navigation.navigate('Dashboard')
        },
        err => {
          alert(err.response.data.message)
        })
  }

  render() {
    return (
      <View style={{ flex: 1, alignContent: 'center', marginTop: 15, padding: 5, paddingTop: 20 }}>
        <Text style={{ fontSize: 18, fontWeight: 'bold', margin: 'auto', paddingBottom: 15 }}>Ingrese sus credenciales para continuar</Text>
        <TextInput 
          style={{marginBottom: 15}}
          mode="outlined"
          label="Nombre de usuario"
          value={this.state.username}
          onChangeText={(username) => this.setState({username})}
        />
        <TextInput
          mode="outlined"
          label="Contraseña"
          secureTextEntry
          style={{marginBottom: 15 }}
          value={this.state.password}
          onChangeText={(password) => this.setState({password})}
        />
        <Button mode="contained" onPress={this.handleLogin} disabled={ this.state.username == '' || this.state.password == '' }>Ingresar</Button>
      </View>
    );
  }
}