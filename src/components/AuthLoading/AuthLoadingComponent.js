import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  View,
} from 'react-native';

export default class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const SessionInfo = await AsyncStorage.getItem('SessionInfo');

    this.props.navigation.navigate(SessionInfo ? 'App' : 'Auth');
  };
  
  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}