import React, { Component } from 'react';
import { View, Text } from 'react-native';
import CRUDComponent from '../CRUDComponent/CRUDComponent'

export default class AppsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  static navigationOptions = {
    title: 'Gestión de apps'
  };

  render() {
    return (
      <View>
        <CRUDComponent
          class={''}
          controller={'app'}
          pk={'_id'}
          clickCallback={() => console.log('Prueba Click Callback')}
          enableSelect={false} //Pendiente implementar (checkbox a la izquierda de las columnas)
          enableCreate={true}
          enableUpdate={true}
          enableDelete={true}
          enableCache={false} //Pendiente implementar
          enableExport={[false, ['csv', 'xml', 'json']]} //Pendiente implementar
          enablePrint={false} //Pendiente implementar
          enableSorting={false} //Pendiente implementar
          enableColumnFilter={false} //Pendiente implementar
          itemsPerPage={10}
          filter={[
            { 'parameterName': 'code', 'parameterValue': '', 'parameterContainer': 'query' },
            { 'parameterName': 'description', 'parameterValue': '', 'parameterContainer': 'query' }
          ]} //Pendiente implementar
          columns={[
            { 'columnName': 'code', 'title': 'Código' },
            { 'columnName': 'description', 'title': 'Descripción' },
            { 'columnName': 'url', 'title': 'URL' }
          ]}
          additionalActions={[]}
        />
      </View>
    );
  }
}
