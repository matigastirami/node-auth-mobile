import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator, createAppContainer } from "react-navigation";
import LoginComponent from '../components/Login/LoginComponent'
import DashboardComponent from '../components/Dashboard/DashboardComponent'
import RolesComponent from '../components/Roles/RolesComponent'
import AuthLoadingComponent from '../components/AuthLoading/AuthLoadingComponent'
import UsersComponent from '../components/Users/UsersComponent'
import AppsComponent from '../components/Apps/AppsComponent'
import navigationOptions from '../navigation/HeaderStyles'
import React from 'react'
import { Platform } from 'react-native'
import Ionicons from "react-native-vector-icons/Ionicons";

const AppStack = createBottomTabNavigator(
	{
		Dashboard: createStackNavigator(
			{
				Dashboard: {
					screen: DashboardComponent
				}
			},
			{
				defaultNavigationOptions: navigationOptions
			}
		),
		Users: createStackNavigator(
			{
				Users: {
					screen: UsersComponent
				}
			},
			{
				defaultNavigationOptions: navigationOptions
			}
		),
		Apps: createStackNavigator(
			{
				Users: {
					screen: AppsComponent
				}
			},
			{
				defaultNavigationOptions: navigationOptions
			}
		),
		Roles: createStackNavigator(
			{
				Users: {
					screen: RolesComponent
				}
			},
			{
				defaultNavigationOptions: navigationOptions
			}
		)
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, horizontal, tintColor }) => {
				const { routeName } = navigation.state;
				let IconComponent = Ionicons;
				let iconName;
				if (routeName === 'Dashboard') {
					iconName = 'md-paper';
					// Sometimes we want to add badges to some icons. 
					// You can check the implementation below.
				} else if (routeName === 'Apps') {
					iconName = 'md-apps';
				} else if (routeName === 'Users') {
					iconName = 'md-people';
				} else if (routeName === 'Roles') {
					iconName = 'md-star';
				}
				// You can return any component that you like here!
				return <Ionicons name={iconName} size={25} color={tintColor} />;
			},
		}),
		tabBarOptions: {
			activeTintColor: 'blue',
			inactiveTintColor: 'gray',
		},
	}
)

//const bottomTabs = createBottomTabNavigator({AppStack})

const AuthStack = createStackNavigator({
	Login: {
		screen: LoginComponent,
		navigationOptions: () => ({
			title: `UsersAdmin`
		})
	}
},
	{
		initialRouteName: "Login",
		defaultNavigationOptions: {
			headerStyle: {
				backgroundColor: '#0261f9',
				paddingEnd: 10
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold'
			}
		}
	}
)

export default createAppContainer(createSwitchNavigator(
	{
		AuthLoading: AuthLoadingComponent,
		App: AppStack,
		Auth: AuthStack,
	},
	{
		initialRouteName: 'AuthLoading',
	}
));