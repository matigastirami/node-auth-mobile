import { deleteFromStorage } from '../Storage/Storage'
import React from 'react'
import {Button} from 'react-native'

export default function ({ navigation, navigationOptions }) {
    return({
        headerLeft: null,
        headerStyle: {
            backgroundColor: '#0261f9'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold'
        },
        headerRight: (
            <Button 
                title="Salir"
                color="black"
                onPress={() => {
                    deleteFromStorage("SessionInfo")
                        .then(() => {
                            console.log("Datos de sesión eliminados satisfactoriamente")
                            navigation.navigate('Login')
                        })
                        .catch(() => {
                            console.log("Ocurrió un error al eliminar los datos de sesión")
                        })
                }}
            />
            )
    })
}