import axios from 'axios';

function onUploadProgress(ev) {
    console.log(ev);
    document.getElementsByClassName("progress").style.display = "block"

    setTimeout(() => {
        document.getElementsByClassName("progress").style.display = "none"
    }, 5000)
    // do your thing here
}

function handleUploadProgress(ev) {
    console.log(ev);
    // do your thing here
}

// Add a request interceptor
const http = axios.create({
    onDownloadProgress: handleUploadProgress
})

export default http;