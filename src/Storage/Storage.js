import { AsyncStorage } from 'react-native';

export async function saveToStorage(itemName, itemValue, isJSON) {
    try {
        await AsyncStorage.setItem(itemName, isJSON ? JSON.stringify(itemValue) : itemValue);
    } catch (error) {
        alert("Error al guardar datos de sesión")
        console.log(error)
    }
}

export async function getFromStorage(itemName) {
    try {
        var value = null

        value = await AsyncStorage.getItem(itemName);

        //console.log("Valor recuperado de storage: ", JSON.parse(value).token)
        
        return value
    } catch (error) {
        alert("Error al obtener datos de sesión")
        console.log(error)
    }
}

export async function deleteFromStorage(itemName) {
    try {
        await AsyncStorage.removeItem(itemName)
    } catch (error) {
        alert("Error al obtener datos de sesión")
        console.log(error)
    }
}